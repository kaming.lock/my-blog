import { useState, useEffect } from "react";

const ImageLoader = ({ namespace, file_name }) => {
  const [file, setFile] = useState("");

  useEffect(() => {
    if (new RegExp("^[^\\/\n ]*$", "g").test(file_name)) {
      import(`../assets/images/${namespace}/${file_name}`)
        .then((res) => {
          setFile(res.default);
        })
        .catch((err) => console.log(err));
    } else {
      setFile(file_name);
    }
  }, [file_name]);

  return file;
};

export default ImageLoader;
