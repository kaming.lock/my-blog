import { useTranslation } from "react-i18next";
import MetaDecorator from "./MetaDecorator";
import MD from "./MD";

const Minshuku = () => {
  const { t } = useTranslation();

  return (
    <div className="Minshuku">
      <MetaDecorator title={t("Minshuku")} description={t("meta_desc")} />
      <h1 className="heading">{` ${t("Minshuku")} `}</h1>
      <div className="minshukuContent">
        <MD
          namespace="minshuku"
          file_name="大阪周遊卡-2-日行程建議_00_tut_tc.md"
        />
      </div>
    </div>
  );
};

export default Minshuku;
