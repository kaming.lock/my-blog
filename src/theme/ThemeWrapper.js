import React, { useState, useEffect } from "react";
import { ThemeContext, themes } from "../context/ThemeContext";

export default function ThemeContextWrapper(props) {
  const [theme, setTheme] = useState(themes.dark);

  function changeTheme(theme) {
    setTheme(theme);
    document.body.classList.add("theme-changed");
    setTimeout(() => {
      document.body.classList.remove("theme-changed");
    }, 1000);
  }

  useEffect(() => {
    switch (theme) {
      case themes.light:
        document.body.classList.remove("dark-content");
        break;
      case themes.dark:
      default:
        document.body.classList.add("dark-content");
        break;
    }
  }, [theme]);

  return (
    <ThemeContext.Provider value={{ theme: theme, changeTheme: changeTheme }}>
      {props.children}
    </ThemeContext.Provider>
  );
}
