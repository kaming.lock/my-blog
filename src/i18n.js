import i18n from 'i18next';
import { initReactI18next } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';
import translationEN from './locales/en/LC_MESSAGES/message.json';
import translationZhCN from './locales/zh_CN/LC_MESSAGES/message.json';
import translationZhTW from './locales/zh_TW/LC_MESSAGES/message.json';

const resources = { 
    en: {
        translation: translationEN
    },
    zh_CN: {
        translation: translationZhCN
    },
    zh_TW: {
        translation: translationZhTW
    }
};

i18n
    .use(initReactI18next)
    .use(LanguageDetector)
    .init({
        fallbackLng: 'en',
        resources,
    });

export default i18n;