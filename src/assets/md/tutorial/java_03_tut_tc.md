java 有好多唔同變量類型

主要有:

`String` (字串)
逢係用雙引號引住嘅都係 String，比如 `"abc"`
`int` (整數)
逢係數字冇小數點嘅都係 int，比如 `1`
`boolean` (布爾值)
係是與否，只可能係 `true` 或者 `false`
`double` (小數)

逢係數字有小數點嘅都可以係`double`，比如 `1.25`

例子:

<CodeBlock language="java">
String a = "abc";
int b = 1;
boolean c = true;
</CodeBlock>
