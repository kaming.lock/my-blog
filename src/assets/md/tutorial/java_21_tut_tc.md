有 d 符號係好特別嘅
比如雙引號 `"` 係用黎引住字串嘅
單引號係俾 `char` 嘅
但係如果我地真係想輸出果隻字，可以點搞呢？
答案就係用 `\\`

<CodeBlock language="java">
String a = "We are the so-called \\"Viking\\" from the north.";
String b = "It's alright.";
</CodeBlock>

黎個反斜號放響前面就話俾 java 聽黎個符號我係要架，當佢字串就 ok 架啦
黎個動作我地叫 escape
比如我地會話 escape 返果雙引號
咁如果我要用果反斜號咁點呀？
答案都係 `\\`
比如咁

<CodeBlock language="java">
String c  = "The character \\\\ is called backslash.";
</CodeBlock>
