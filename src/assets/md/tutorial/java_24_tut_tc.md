`do/while` 句法係咁

<CodeBlock language="java">
do {
  // do something
} while (condition1);
</CodeBlock>

其實同 `while` 係一樣
只係佢其實會點都行一次入面嘅野先
先再 check 果 `condition1` 係咪 true
係 `true` 先繼續迴圈
比如

<CodeBlock language="java">
int i = 0;
do {
  System.out.println(i);
  i++;
} while (i < 5);
</CodeBlock>

同 `while` 一樣，冇左果 `i++` 會令到果 code 陷入無限迴圈
