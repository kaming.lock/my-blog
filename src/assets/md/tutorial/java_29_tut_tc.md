function 入面其實可以 call 返自己黎用
黎個做法好廣泛有好處唔好處
好處
對付一d較為複雜嘅問題，比如樹狀結構咁，會比較簡單同簡潔
唔好處
慢
用多d記憶體
用返 [Java 25)](/programming/tutorial/java/25) 做例子
比如如果就咁用 `for`

<CodeBlock language="java">
public static int fib(int n) {
  int num1, num2, num3;
  num1 = num2 = num3 = 1;
  for (int i = 3; i <= n; i++) {
    num3 = num1 + num2;
    num1 = num2;
    num2 = num3;
  }
  return num3;
}
</CodeBlock>

但係如果用 function call 返自己就係咁

<CodeBlock language="java">
public static int fib(int n) {
  if ((n == 1) || (n == 2)) return 1;
  else return (fib(n - 1) + fib(n - 2));
}
</CodeBlock>

可以見到咁 call 係簡潔好多但係慢好多