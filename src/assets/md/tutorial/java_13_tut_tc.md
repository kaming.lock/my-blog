邏輯運算符其實就即係邏輯閘入面嘅 AND、OR 同 NOT

AND `&&`
OR `||`
NOT `!`

比如:

<CodeBlock language="java">
boolean a = true;
boolean b = false;
boolean c = a && !b;
</CodeBlock>

咁 `c` 就係 `true`
因為 `a` 係 `true`
`b` 係 `false`，但係 NOT `b` 即係 `false` 嘅相反，即係 `true`
咁 `a && !b` 即係 `true` AND `true`，即係 `true`
