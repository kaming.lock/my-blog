所有銀行賬戶都有賬戶號碼、餘額和轉賬記錄。 Tom (賬戶號碼: 12345435)、Jeff (賬戶號碼: 91554630) 和 Sam (賬戶號碼: 66523301) 各有一個銀行賬戶。 Tom 有 $100，Jeff 有 $200，Sam有 $0。 Tom 將 $50 轉給 Sam ，Sam 將 $25 轉給 Jeff。 請編寫一個程序模擬這種情況並在最後輸出他們的帳號、餘額和轉賬記錄。

已提供以下文件。 您不能修改此文件，但可以在同一目錄中創建一個新文件。

Main.java

<CodeBlock language="java">
public class Main {
  public static void main(String[] args) {
    BankAccount Tom = new BankAccount("12345435");
    BankAccount Jeff = new BankAccount("91554630");
    BankAccount Sam = new BankAccount("66523301");

    Tom.save(100);
    Jeff.save(200);

    Tom.transfer(Sam, 50);
    Sam.transfer(Jeff, 25);

    Tom.showDetails();
    Jeff.showDetails();
    Sam.showDetails();
  }
}
</CodeBlock>