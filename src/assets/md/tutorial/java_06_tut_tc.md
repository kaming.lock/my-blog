當有唔同資料類型加埋一齊嘅時候就會出現隱性轉型
意思即係 java 背地裏幫你轉左個類型
比如:

<CodeBlock language="java">
int a = 1;
int b = 2;
String c = "Tom has ";
String d = " apples";
String e = c + (a + b) + d;
</CodeBlock>

響`e`個位做左括號先
即係`a+b`
因為 2 個都係整數所以出整數
所以`(a+b)`= 3
之後由左至右
`c`係字串
`d`都係字串
`c + (a + b) + d`即係字串+數字+字串
逢係有野加字串最後都一定係出字串
所以`e`係字串
