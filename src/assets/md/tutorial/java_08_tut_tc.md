逢係要比較一 D 野嘅時候都一定會用到
比較完出黎嘅一定係 `boolean`

有:

`==` (是否等於)
`!=` (是否不等於)
`<` (是否小於)
`>` (是否大於)
`<=` (是否小於或等於)
`>=` (是否大於或等於)

比如:

<CodeBlock language="java">
int a = 3;
int b = 4;
boolean c = a >= b;
</CodeBlock>
