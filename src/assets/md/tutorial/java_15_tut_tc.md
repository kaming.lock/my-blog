三元運算符其實係差唔多等同於 `if` 嘅縮寫
比如:

<CodeBlock language="java">
int hour = 20;
String result = (hour < 18) ? "Good day." : "Good evening.";
System.out.println(result);
</CodeBlock>

其實係同下面用 `if` 咁樣係一樣:

<CodeBlock language="java">
int hour = 20;
if (hour < 18) {
  System.out.println("Good day.");
} else {
  System.out.println("Good evening.");
}
</CodeBlock>
