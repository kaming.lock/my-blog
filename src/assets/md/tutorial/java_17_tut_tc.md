`for` 嘅意思係響指定嘅情況下不斷重複某個 code，即係迴圈 (loop)
`for` 嘅句法係咁:

<CodeBlock language="java">
for (statement1; statement2; statement3) {
  // do something
}
</CodeBlock>

其中 `statement1` 係用黎俾你定義一開始嘅變量同值
`statement2` 係話俾 java 知響咩條件之下繼續重複行入面段 code
最後 `statement3` 就係話每次要遞增或者遞減果變量
實際用會好似咁樣

<CodeBlock language="java">
for (int i = 0; i <= 10; i+=2) {
  System.out.println(i);
}
</CodeBlock>

咁 e 段野就會由 `i` 係 0 開始，每次 +2，重複行直到 i 唔係小過或者等於 10 先停
所以佢會輸出 0 - 10 嘅所有雙數出黎

---

## 讀者分享

### GamingShow:

<CodeBlock language="java">
//statement1: i 係用於E個for loop 基本上都係用i
//statement2: 條件
//statement3: 完Loop做既/做完先Loop 取決於 i++/++i
for (int i = 0; i < 10; i++){
  System.out.println(i);
}
</CodeBlock>

上邊 段 code
第一輪 `i = 0` ，然後 判定 `i` 是否 細於 10 (因為係 0 所以是)，咁就會 Load 一次內容 `System.out.println(i);` ，而 `i` 係 0 所以會 Output 0 ，然後就 `i++` (i = i + 1)

第二輪 `i = 1` ，然後 判定 `i` 是否 細於 10 (因為係 1 所以是)，咁就會 Load 一次內容 `System.out.println(i);` ，而 `i` 係 1 所以會 Output 1，然後就 `i++` (i = i + 1)

第三輪 `i = 2` ，然後 判定 `i` 是否 細於 10 (因為係 2 所以是)，咁就會 Load 一次內容 `System.out.println(i);` ，而 `i` 係 2 所以會 Output 2，然後就 `i++` (i = i + 1)
.
.
.
第十一輪 `i` = 10 ，然後 判定 `i` 是否 細於 10 (因為係 10 所以否)，咁就完

<CodeBlock language="java">
//statement1: i 唔一定係要用0 亦都可以用外部既數值/起始就係其他數
//statement2: 條件
//statement3: 完Loop做既/做完先Loop 取決於 i++/++i
int x = 5;
for (int i = x; i < 10; i++) {
  System.out.println(i);
}
</CodeBlock>

上邊 段 code
第一輪 `i = x` (而 x = 5) 所以 `i = 5`，然後 判定 `i` 是否 細於 10 (因為係 5 所以是)，咁就會 Load 一次內容 `System.out.println(i);` ，而 `i` 係 5 所以會 Output 5 ，然後就 `i++` (i = i + 1)

第二輪 `i = 6` ，然後 判定 `i` 是否 細於 10 (因為係 6 所以是)，咁就會 Load 一次內容 `System.out.println(i);` ，而 `i` 係 6 所以會 Output 6，然後就 `i++` (i = i + 1)

第三輪 `i = 7` ，然後 判定 `i` 是否 細於 10 (因為係 7 所以是)，咁就會 Load 一次內容 `System.out.println(i);` ，而 `i` 係 7 所以會 Output 7，然後就 `i++` (i = i + 1)
.
.
第六輪 `i = 10` ，然後 判定 `i` 是否 細於 10 (因為係 10 所以否)，咁就完
