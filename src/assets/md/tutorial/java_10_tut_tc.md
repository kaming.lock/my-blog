一元運算符有分遞增 (Increment) 同遞減 (Decrement)
遞增即係不斷增加
遞減即係不斷減少
其中遞增又有分預增 (Pre-Increment) 同後增 (Post-Increment)
遞減亦都係一樣
有預減 (Pre-Decrement) 同後減 (Post-Decrement)
預同後果分別只係差在做先定做後，冇其他分別
預就係做先
後就係做後
但佢地嘅意思都係一樣
就係每次 +1 或者每次 -1
遞增果符號係咁樣: `++`
遞減果符號係咁樣: `--`
如果要做先就果變量響後面
比如咁樣:

<CodeBlock language="java">
++x;
</CodeBlock>

變量放前面就係做後

<CodeBlock language="java">
x++;
</CodeBlock>

遞減同理

<CodeBlock language="java">
--x;
x--;
</CodeBlock>

宜家睇下以下黎個例子:

<CodeBlock language="java">
int x = 1;
++x;
int y = ++x;
</CodeBlock>

第一行定義左整數 `x` 係 1
第二行遞增 `x`，所以到黎個位 `x` 係 2
第三行定義左整數 `y` 係遞增 `x`
而因為黎個遞增係預遞增
所以佢係遞增完 `x` 先再定義 `x` 俾 `y`
咁最後 `x` 就會係 3 而 `y` 就會係 3
如果改成後遞增

<CodeBlock language="java">
int x = 1;
x++;
int y = x++;
</CodeBlock>

第三行就會變成定義左 `x` 俾 `y` 先再遞增 `x`
咁最後 `x` 就會係 3 而 `y` 就會係 2
