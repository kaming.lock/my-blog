`while` 同 `for` 差唔多都係迴圈嘅一種
佢果句法係咁

<CodeBlock language="java">
while (condition1) {
  // do something
}
</CodeBlock>

同 `for` 唔同就係佢冇話一定要一個遞增或者遞減嘅宣言
只要果 `condition1` 一日係 `true` 都會繼續迴圈
比如:

<CodeBlock language="java">
while (i < 5) {
  System.out.println(i);
  i++
}
</CodeBlock>

咁佢就會輸出 0-4
如果冇左 `i++` 果句，java 就會迴圈到佢頂唔順為止
然後就會鬧 error
