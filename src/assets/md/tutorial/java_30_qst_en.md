All bank account has a account number, balance and transfer record. Tom (Acc No: 12345435), Jeff  (Acc No: 91554630)and Sam (Acc No: 66523301)each have a bank account. Tom has $100, Jeff has $200 and Sam has $0. Tom has transferred $50 to Sam and Sam has transferred $25 to Jeff. Please create a program that simulate such situation and output their account number, balance and transfer record at the end.

The following file has been given. You are not allowed to modify this file but to create a new file in the same directory.

Main.java

<CodeBlock language="java">
public class Main {
  public static void main(String[] args) {
    BankAccount Tom = new BankAccount("12345435");
    BankAccount Jeff = new BankAccount("91554630");
    BankAccount Sam = new BankAccount("66523301");

    Tom.save(100);
    Jeff.save(200);

    Tom.transfer(Sam, 50);
    Sam.transfer(Jeff, 25);

    Tom.showDetails();
    Jeff.showDetails();
    Sam.showDetails();
  }
}
</CodeBlock>