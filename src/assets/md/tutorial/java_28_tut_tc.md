一個 code block 其實就係用 `{` 加 `} ` 包住就係
比如如果你回想返之前教 `if` `switch` `for` `while` 甚至 `function` 都有 code block
所以 code block 嘅前同後其實係唔會加 `;`
因為 `;` 代表黎句停止
同一時間亦要知道響 code block 入面定義嘅變數響出面係用唔到

<CodeBlock language="java">
if (true) {
  int x = 100;
}
System.out.println(x);
// System.out.println(x);
//                    ^
//  symbol:   variable x
//  location: class Main
// 1 error
</CodeBlock>

咁就會鬧
值得注意嘅係 for 果 statement 入面係會定義一個變量 （多數係 i）
黎個變量係響佢自己果 code block 入面都用到
當然 code block 亦可以就咁用
原理一樣都係入面定義嘅變量出面用唔到

<CodeBlock language="java">
{
  int x = 100;
}
System.out.println(x);
// System.out.println(x);
//                    ^
//  symbol:   variable x
//  location: class Main
// 1 error
</CodeBlock>

咁就會鬧
