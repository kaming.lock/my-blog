Java 係一個 Object-Oriented Programming Language (OOP) 嘅語言黎
即係話 java 入面嘅所有野都係同類別 (Class)、物件 (Object)、屬性 (Attributes) 同方法 (Method) 都係互相關聯
比如車係一個類別黎
如果我話黎度有 3 架車就係話黎度有 3 個物件
咁每架車都佢自己嘅屬性
比如佢幾重、咩色、咩牌子
跟住佢可以 d engine 或者 brake 就係佢嘅方法
即係架車可以踩 brake 黎 stop 架車
而點樣整一個 `class` 呢？
一個 `class` 嘅格式係咁樣

```
<access> <non-access> class <name> {
  // do something
}
```

`<access>` 暫時無視佢先，淨係知道有 `public` 就 ok

`<non-access>` 暫時無視佢先

`<name>` 就係 class 果名，通常第一個字大階
比如Car 而唔係 car
又比如 NumberGenerator 而唔係 number generator
全部字大階開頭
之後唔可以有空白鍵
有就痴埋一齊

一個 `class` 入面可以有好多屬性
比如:
Car.java

<CodeBlock language="java">
public class Car {
  double weight = 0;
  String color = "black";
  String brand = "Toyota";
}
</CodeBlock>


一個 `class` 入面亦可以有好多方法
加埋上面嘅就會好似咁樣:
Car.java

<CodeBlock language="java">
public class Car {
  double weight = 0;
  String color = "black";
  String brand = "Toyota";

  public static void drive() {
    System.out.println("driving...");
  }

  public static void stop() {
    System.out.println("stopped");
  }
}
</CodeBlock>


一個 file 通常只會有一個 `class`
比如你果 `class` 叫 Car
咁你果 file 就要叫 Car.java
要同名
所以上面個例子個 file 就係 Car.java
咁如果我地用開 Main.java
想用返 Car.java 入面嘅野
咁 d 2 個 file 就要係響同一個 folder
所以 Car.java 同 Main.java 係響同一個 folder
宜家返返黎 Main.java
Main.java

<CodeBlock language="java">
public class Main {
  public static void main(String[] args) {
    // TODO
  }
}
</CodeBlock>


要用 Car.java 入面嘅野就係咁:
Main.java

<CodeBlock language="java">
public class Main {
  public static void main(String[] args) {
    Car a = new Car();
  }
}
</CodeBlock>

黎句

<CodeBlock language="java">
Car a = new Car();
</CodeBlock>

就係根據 `Car` 黎個類別整一個新嘅物件
果類型就跟返 `class` 果名
果 `new` 就係新嘅物件嘅意思
而個物件就係 Car
咁 `a` 黎個變量就係一架車黎

跟住可以拎到架車嘅屬性

<CodeBlock language="java">
System.out.println(a.brand);
</CodeBlock>

亦可以用佢嘅方法

<CodeBlock language="java">
a.drive();
</CodeBlock>

咁就開車了
回想返由第一堂學嘅 print

<CodeBlock language="java">
System.out.println("abc");
</CodeBlock>

其實係 `System` 入面有 `out` 黎個 object，而 `out` 黎個 object 有 `print`、`println` d 2 個方法
跟住 pass 左 `abc` 黎個字串入去果 function 入面等佢輸出