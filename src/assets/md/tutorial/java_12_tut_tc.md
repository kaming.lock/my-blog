賦值運算符有好種:

`+=`
`-=`
`*=`
`/=`
`%=`
`&=`
`|=`
`^=`
`<<=`
`>>=`

其實只係將左手邊嘅值同右手邊嘅值經過運算後賦予返俾左手邊嘅變量

比如:

<CodeBlock language="java">
int a = 9;
a+=2;
</CodeBlock>

咁 `a` 就會係 11
其實係同以下咁樣係冇分別，只係縮寫

<CodeBlock language="java">
int a = 9;
a = a + 2;
</CodeBlock>

`a` 都會係 11
