java 可以做到普通嘅數學加減乘除
例子:

<CodeBlock language="java">
int a = 1;
int b = 2;
int c = a + b;
int d = a - b;
int e = 3 * 10;
int f = 9 / 2;
</CodeBlock>

值得留意嘅係 `f`
佢唔係出 4.5 而係 4
咁係因為 java 嘅整數除整數只會出整數而唔會出小數
如果要小數就要小數除小數
而餘數可以咁做

<CodeBlock language="java">
int g = 9 % 2;
</CodeBlock>

黎個意思即係 9÷2 嘅餘數即係 1
