輸出句法主要有 2 種:

1. 第一種

   <CodeBlock language="java">
   System.out.print("abc");
   </CodeBlock>

2. 第二種

   <CodeBlock language="java">
   System.out.println("abc");
   </CodeBlock>

第一種會響同一句輸出
<br>
第二種就係會響輸出之後開一行新
<br>
當然你亦可以用`\`嘅方式去隔行

<CodeBlock language="java">
System.out.print("abc\n");
</CodeBlock>

效果係同第二種嘅一樣
一般建議使用第二種就可以了
