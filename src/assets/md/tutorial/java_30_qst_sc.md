所有银行账户都有账户号码、余额和转账记录。 Tom (账户号码: 12345435)、Jeff (账户号码: 91554630) 和 Sam (账户号码: 66523301) 各有一个银行账户。 Tom 有 $100，Jeff 有 $200，Sam有 $0。 Tom 将 $50 转给 Sam ，Sam 将 $25 转给 Jeff。请编写一个程序模拟这种情况并在最后输出他们的帐号、余额和转账记录。 

已提供以下文件。您不能修改此文件，但可以在同一目录中创建一个新文件。 

Main.java

<CodeBlock language="java">
public class Main {
  public static void main(String[] args) {
    BankAccount Tom = new BankAccount("12345435");
    BankAccount Jeff = new BankAccount("91554630");
    BankAccount Sam = new BankAccount("66523301");

    Tom.save(100);
    Jeff.save(200);

    Tom.transfer(Sam, 50);
    Sam.transfer(Jeff, 25);

    Tom.showDetails();
    Jeff.showDetails();
    Sam.showDetails();
  }
}
</CodeBlock>