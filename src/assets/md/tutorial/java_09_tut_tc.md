`if` 嘅句法係:

<CodeBlock language="java">
if (condition1) {
  // do something when condition1 is true
} else if (condition2) {
  // do something when condition2 is true
} else if (condition3) {
  // do something when condition3 is true
} else {
  // do something if none of them are true
}
</CodeBlock>

其中 `condition1`, `condition2` 同 `condition3` 一定要係 `boolean` 黎

比如:

<CodeBlock language="java">
int a = 1;
int b = 2;
if (a < b) {
  System.out.println("a is smaller than b");
}
</CodeBlock>
