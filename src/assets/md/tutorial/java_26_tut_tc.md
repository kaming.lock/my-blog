一個 function 都可以有好多個參數
比如:

<CodeBlock language="java">
public static void printName(String name, int age) {
  System.out.println("My name is " + name + " and I am " + age + " years old.");
}

public static void main(String[] args) {
  printName("Amy", 14);
}
</CodeBlock>
