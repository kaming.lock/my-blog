Matrix is a rectangular array of numbers. The size of a matrix is represented as `m` x `n`. For example, you can have a 3 x 2 matrix. That means the matrix has 3 rows and 2 columns. Matrixes can be multiplied like normal maths but there is exception where they cannot be multiplied. For them to be able to multiplied, `the number of columns in the first matrix` has to equals to `the number of rows in the second matrix`. The size of the resulted matrix is always `the number of rows in the first matrix` x `the number of columns in the second matrix`. For example, if `A` as 1x3 matrix multiply by `B` as 3x2 matrix, it will get a 1x2 matrix. But `B` cannot multiply by `A`. When multiplying matrix, you will multiply the first row in first matrix by the first column in second matrix, add them up to be the first value. Then you multiply the first row in first matrix by the second column in second matrix, add them up to be the second value and so on. Please see the following example:

```
A = 3 4
    7 2
    5 9

B = 3 1 5
    6 9 7

A is 3x2 matrix and B is 2x3 matrix, so they can be multiplied. The resulted matrix has to be a 3x3 matrix.

(3)(3) + (4)(6) = 9 + 24 = 33
(3)(1) + (4)(9) = 3 + 36 = 39
(3)(5) + (4)(7) = 15 + 28 = 43
(7)(3) + (2)(6) = 21 + 12 = 33
(7)(1) + (2)(9) = 7 + 18 = 25
(7)(5) + (2)(7) = 35 + 14 = 49
(5)(3) + (9)(6) = 15 + 54 = 69
(5)(1) + (9)(9) = 5 + 81 = 86
(5)(5) + (9)(7) = 25 + 63 = 88

A·B = 33 39 43
      33 25 49
      69 86 88
```

It is given `A` is a 3x3 matrix and `B` is a 3x2 matrix like below. Please make a program to calculate and output the results of `A` · `B` and `B` · `A`. Shows `it cannot be multiplied` if they cannot.

```
A = 9 5 2
    1 8 5
    3 1 6

B = 3 2
    1 4
    5 3
```

(`A` · `B` means `A` multiply by `B`)