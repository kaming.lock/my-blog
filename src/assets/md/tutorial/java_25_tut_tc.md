由第一堂一直無視到宜家果句終於要揭曉啦

<CodeBlock language="java">
public static void main(String[] args) {
</CodeBlock>

到底點解呢？
其實黎個係一個 function (函式) 黎
我地之前打嘅所有 code 都係要響 function 入面先會 work 到
宜家黎睇下黎句點解啦
function 嘅句法係咁

```
<access> <non-access> <type> <name>(<parameter>) {
    // do something
}
```

`<access>` 暫時無視佢先，淨係知道有 `public` 就 ok

`<non-access>` 暫時無視佢先，淨係知道有 `static` 就 ok

`<type>` 其實即係 function type (函式型)
我地見到一開始就已經寫左 `void`
`void` 中文即係虛空
咩係虛空呢？
咁就要理解 java 嘅 function
響 java 入面，function 亦可以做好似 pascal 或者好多其他 language 入面嘅 procedure 咁
咁 pascal 嘅 procedure 同 function 有咩分別呢？
其實 pascal 入面，行完一個 function 一定要俾返個果個 function 嘅輸出，但係 procedure 就唔需要，因為佢只係一個流程，行完就 ok
而返返黎 java，java 稱呼 d 2 種都叫 function
而點樣分果個 function 到底有冇野輸出
如果函數型係 `void` 即係唔會有輸出
函數型除左 `void` 仲有其他類型
比如 `int`, `String` 果 d
所有變量類型響函數型都可以用
但如果唔係 `void` 就要輸出返對應嘅函數型
而輸出嘅手段就係 `return`
比如咁

<CodeBlock language="java">
public static String getName() {
  return "abc";
}
</CodeBlock>

咁即係話黎個 function 最後係會輸出一個字串
如果輸出嘅唔係字串 java 就會鬧

`<name>` 其實係 function 嘅名黎
同變量一樣都係唔可以重複定義同一個名

`<parameter>` 其實係俾你 pass 一 d 變量入去果 function 用，可以冇 parameter 都 ok
可以見到一開始果個 function 已經有預設嘅 `String[] args` 響度
如果對 terminal 有認知嘅話，其實佢係俾用 terminal 嘅人 pass 一 d 值入去
但係如果你係整一個新嘅 function ，parameter 唔係一定要，空左佢都 ok

如果要用果 function 就係好似咁樣

<CodeBlock language="java">
getName();
</CodeBlock>

比如要定義落一個變量可以咁樣

<CodeBlock language="java">
String name = getName();
</CodeBlock>

最後記住唔好改左 main 果 function 個名佢，因為 java 預設會行左 main 果 function 先
以下係實例

<CodeBlock language="java">
public static String getName() {
  return "tom";
}

public static void printName(String name) {
  System.out.println("My name is " + name);
}

public static void main(String[] args) {
  String lastname = "hardy";
  printName(getName() + " " + lastname);
}
</CodeBlock>
