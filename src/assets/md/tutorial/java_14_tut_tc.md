`switch` 其實同 `if` 差唔多
下面係 `switch` 嘅句法

<CodeBlock language="java">
switch (variable) {
  case value1:
    // do something when variable equals to value1
    break;
  case value2:
    // do something when variable equals to value2
    break;
  default:
    // do something if none matches
    break;
}
</CodeBlock>

可以睇到 `switch` 其實係唔洗俾一個 `condition` (條件)
而係要一個 `variable` (變量) 就 ok
係更多針對一個變量去做 checking
所以如果你有多一個變量要 check 咁 `switch` 可能就唔岩你喇
用 `if` 好 d
但如果你只係需要 check 一個變量咁 `switch` 係唔錯嘅選擇
下面係實際用 `switch` 嘅例子左參考

<CodeBlock language="java">
int a = 9;
switch (a) {
  case 1:
    System.out.println("1");
    break;
  case 2:
    System.out.println("2");
    break;
  default:
    System.out.println("not 1 and not 2");
    break;
}
</CodeBlock>

可以見到其實用 `if` 都做得到

<CodeBlock language="java">
int a = 9;
if (a == 1) {
  System.out.println("1");
} else if (a == 2) {
  System.out.println("2");
} else {
  System.out.println("not 1 and not 2");
}
</CodeBlock>
