The following java program is given:

<CodeBlock language="java">
System.out.println("This is a useless line");
 
for (int i = 0; i < 5; i++) {
    System.out.println("This is another useless line: " + i);
}
</CodeBlock>

Please help to comment out all the useless lines from the program.