Tommy 製作了一個類似下面的 java 程序：

<CodeBlock language="java">
public static void main(String[] args) {
  System.out.println("Hello, World!");
  {
    int x = 100;
  }
  System.out.println(x);
}
</CodeBlock>

他在嘗試運行他的程序時遇到錯誤。請幫助他修復他的程序並輸出 `x` 的值。