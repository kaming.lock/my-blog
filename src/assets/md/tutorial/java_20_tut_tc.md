`continue` 嘅用法就唔同 `break`
唔係直接完左果個迴圈
而係只跳過果一個迴圈
比如

<CodeBlock language="java">
for (int i = 0; i < 10; i++) {
  if (i == 4) {
    continue;
  }
  System.out.println(i);
}
</CodeBlock>

咁佢就會出 1-3 ，跳過 4，之後出 5-9
