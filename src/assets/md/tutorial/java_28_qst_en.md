Tommy has made a java program like the one below:

<CodeBlock language="java">
public static void main(String[] args) {
  System.out.println("Hello, World!");
  {
    int x = 100;
  }
  System.out.println(x);
}
</CodeBlock>

He is getting an error when trying to run his program. Please help him to fix his program and output the value of `x`.