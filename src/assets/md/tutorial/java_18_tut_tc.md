數組其實可以入面再包數組
好似咁樣:

<CodeBlock language="java">
int a[][] = {{1,1,1}, {2,2,2}, {3,3,3}};
</CodeBlock>

又或者咁樣就係定義一個空嘅 3x3 數組

<CodeBlock language="java">
int emptyArr[][] = new int[3][3];
</CodeBlock>
