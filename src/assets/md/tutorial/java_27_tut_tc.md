雖然之前有講過 function 唔可以同名
但係其實係可以嘅
只要係入唔同嘅參數就可以
比如

<CodeBlock language="java">
public static String fullname(String firstname) {
  return firstname;
}

public static String fullname(String firstname, String lastname) {
  return firstname + " " + lastname;
}

public static void main(String[] args) {
  System.out.println(fullname("tom"));
  System.out.println(fullname("tom", "hardy"));
}
</CodeBlock>
