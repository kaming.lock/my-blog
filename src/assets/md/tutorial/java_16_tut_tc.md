Array (數組) 係一個變量類型黎
例如:

`String` 嘅數組就係 `String[]`
`int` 嘅數組就係 `int[]`
`double` 嘅數組就係 `double[]`
`boolean` 嘅數組就係 `boolean[]`

數組意思就係一抽野
比如 `int[]` 就係一抽整數

例如:

<CodeBlock language="java">
int[] nums = {10, 20, 30, 40};
</CodeBlock>

如果要定義一個空嘅數組就係咁樣:

<CodeBlock language="java">
int[] emptyArr = new int[4];
</CodeBlock>

其中 4 係果數組嘅長度
如果要拎返某個出黎用
只需要知道佢個位置就 ok
位置由 0 開始數
比如

<CodeBlock language="java">
int num1 = nums[0];
</CodeBlock>

咁 `num1` 就係 10
如果

<CodeBlock language="java">
int num2 = nums[2];
</CodeBlock>

咁 `num2` 就係 30
如果嘗試拎超過數組長度嘅位置咁 java 就會鬧你

比如:

<CodeBlock language="java">
System.out.println(nums[5]);
 
// Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 5 out of bounds for length 4 at Class.main (Class.java:9)
</CodeBlock>

所以響 java 入面，你一定義左數組，佢果長度就定死左改唔到
當然你可以拎到數組嘅長度

<CodeBlock language="java">
int length = nums.length;
</CodeBlock>

亦都可以改變某個位置嘅值

<CodeBlock language="java">
nums[0] = 0;
</CodeBlock>
