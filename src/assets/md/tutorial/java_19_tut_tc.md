`break` 嘅用法係響迴圈入面可以直接中斷果個迴圈
比如:

<CodeBlock language="java">
for (int i = 0; i < 10; i++) {
if (i == 4) {
  break;
}
  System.out.println(i);
}
</CodeBlock>

咁佢就會響輸出完 0-3 之後直接完左果迴圈
但係佢冇辨法 `break` 到 Nested Loop (嵌套迴圈)
比如:

<CodeBlock language="java">
for (int i = 0; i < 5; i++) {
  for (int j = 0; j < 5; j++) {
    if (i== 3) {
      break;
    }
    System.out.println(i + ", " + j);
  }
}
</CodeBlock>

咁佢只會 break 返佢所在嘅果個迴圈
跳過左 `i=3` 果一次迴圈
仲係會響 `i=4` 繼續
