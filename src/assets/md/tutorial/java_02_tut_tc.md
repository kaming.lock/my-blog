Variable 即係變量
可以想像係數學入面嘅代數
即係 d 公式入面嘅英文字母
例如你可以話 `x=1` 即係講緊 `x` 黎個代數係 1
同數學堂唔同，響 java 入面你需要話埋佢知 `x` 係咩類型
如果 `x=1` 嘅話，咁即係 `x` 係數字黎，而且係整數
咁我就要響前面加返
整數英文係 Integer，java 入面係叫 `int`
所以如果要整一個變量就係

<CodeBlock language="java">
int x = 1;
</CodeBlock>

咁就同 java 宣告左黎個變量
