Tommy 制作了一个类似下面的 java 程序：

<CodeBlock language="java">
public static void main(String[] args) {
  System.out.println("Hello, World!");
  {
    int x = 100;
  }
  System.out.println(x);
}
</CodeBlock>

他在尝试运行他的程序时遇到错误。请帮助他修复他的程序并输出 `x` 的值。