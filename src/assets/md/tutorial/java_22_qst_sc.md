给出以下java程序：

<CodeBlock language="java">
System.out.println("This is a useless line");
 
for (int i = 0; i < 5; i++) {
    System.out.println("This is another useless line: " + i);
}
</CodeBlock>

请帮助注释掉程序中所有无用的行。